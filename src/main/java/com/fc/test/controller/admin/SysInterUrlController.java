package com.fc.test.controller.admin;

import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.fc.test.common.base.PageInfo;
import com.fc.test.dao.model.SysInterUrlModel;
import com.fc.test.dao.service.SysInterUrlService;
import com.fc.test.model.custom.TableSplitResult;
import com.fc.test.util.StringUtils;
import lombok.RequiredArgsConstructor;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fc.test.common.base.BaseController;
import com.fc.test.common.domain.AjaxResult;
import com.fc.test.model.custom.Tablepar;
import com.fc.test.model.custom.TitleVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "拦截url表")
@Controller
@RequestMapping("/SysInterUrlController")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SysInterUrlController extends BaseController {

    private String prefix = "admin/sysInterUrl";
    @Autowired
    private SysInterUrlService sysInterUrlService;

    /**
     * 分页跳转
     *
     * @param model
     * @return
     * @author fuce
     * @Date 2020年4月18日 下午11:43:33
     */
    @ApiOperation(value = "分页跳转", notes = "分页跳转")
    @GetMapping("/view")
    @RequiresPermissions("gen:sysInterUrl:view")
    public String view(ModelMap model) {
        String str = "拦截url表";
        setTitle(model, new TitleVo("列表", str + "管理", true, "欢迎进入" + str + "页面", true, false));
        return prefix + "/list";
    }

    /**
     * 分页查询
     *
     * @param tablepar
     * @param searchText
     * @return
     * @author fuce
     * @Date 2020年4月18日 下午11:43:42
     */
    //@Log(title = "拦截url表集合查询", action = "111")
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @PostMapping("/list")
    @RequiresPermissions("gen:sysInterUrl:list")
    @ResponseBody
    public Object list(Tablepar tablepar, String searchText) {
        PageInfo<SysInterUrlModel> page = sysInterUrlService.listTablepar(tablepar, "inter_name", searchText);
        return new TableSplitResult<SysInterUrlModel>(page.getPageNum(), page.getTotal(), page.getList());
    }

    /**
     * 新增跳转
     */
    @ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap) {
        return prefix + "/add";
    }

    /**
     * 新增
     *
     * @param sysInterUrl
     * @return
     * @author fuce
     * @Date 2020年4月18日 下午11:44:30
     */
    //@Log(title = "拦截url表新增", action = "1")
    @ApiOperation(value = "新增", notes = "新增")
    @PostMapping("/add")
    @RequiresPermissions("gen:sysInterUrl:add")
    @ResponseBody
    public AjaxResult add(SysInterUrlModel sysInterUrl) {
        return toAjax(sysInterUrlService.save(sysInterUrl));
    }

    /**
     * 复制
     *
     * @param id
     * @return
     * @author fuce
     * @Date 2020年4月18日 下午11:44:42
     */
    //@Log(title = "复制", action = "1")
    @ApiOperation(value = "复制", notes = "复制")
    @GetMapping("/copy/{id}")
    @RequiresPermissions("gen:sysInterUrl:add")
    @ResponseBody
    public AjaxResult add(@PathVariable("id") String id) {
        SysInterUrlModel sysInterUrl = sysInterUrlService.getById(id);
        sysInterUrl.setInterName(sysInterUrl.getInterName() + "复制");
        return toAjax(sysInterUrlService.save(sysInterUrl));
    }


    /**
     * 删除用户
     *
     * @param ids
     * @return
     */
    //@Log(title = "拦截url表删除", action = "111")
    @ApiOperation(value = "删除", notes = "删除")
    @PostMapping("/remove")
    @RequiresPermissions("gen:sysInterUrl:remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(sysInterUrlService.removeByIds(StringUtils.splitList(ids, StringPool.COMMA)));
    }

    /**
     * 检查用户
     *
     * @return
     */
    @ApiOperation(value = "检查Name唯一", notes = "检查Name唯一")
    @PostMapping("/checkNameUnique")
    @ResponseBody
    public int checkNameUnique(SysInterUrlModel sysInterUrl) {
        int b = sysInterUrlService.checkNameUnique("inter_name", sysInterUrl.getInterName());
        return b > 0 ? 1 : 0;
    }


    /**
     * 修改跳转
     *
     * @param id
     * @param mmap
     * @return
     */
    @ApiOperation(value = "修改跳转", notes = "修改跳转")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap) {
        mmap.put("SysInterUrl", sysInterUrlService.getById(id));
        return prefix + "/edit";
    }

    /**
     * 修改保存
     */
    //@Log(title = "拦截url表修改", action = "1")
    @ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("gen:sysInterUrl:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(SysInterUrlModel record) {
        return toAjax(sysInterUrlService.updateById(record));
    }


    /**
     * 根据主键查询
     *
     * @param id
     * @return
     */
    @PostMapping("/get/{id}")
    @ApiOperation(value = "根据id查询唯一", notes = "根据id查询唯一")
    public SysInterUrlModel edit(@PathVariable("id") String id) {
        return sysInterUrlService.getById(id);
    }


}
