package com.fc.test.controller.admin;

import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.fc.test.common.base.PageInfo;
import com.fc.test.dao.model.SysStreetModel;
import com.fc.test.dao.service.SysStreetService;
import com.fc.test.model.custom.TableSplitResult;
import com.fc.test.util.StringUtils;
import lombok.RequiredArgsConstructor;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fc.test.common.base.BaseController;
import com.fc.test.common.domain.AjaxResult;
import com.fc.test.model.custom.Tablepar;
import com.fc.test.model.custom.TitleVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 街道Controller
 *
 * @author fuce
 * @ClassName: SysStreetController
 * @date 2019-11-20 22:32
 */
@Api(value = "街道设置")
@Controller
@RequestMapping("/SysStreetController")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SysStreetController extends BaseController {

    private String prefix = "admin/province/sysStreet";
    private final SysStreetService sysStreetService;

    /**
     * 展示跳转
     *
     * @param model
     * @return
     */
    @ApiOperation(value = "分页跳转", notes = "分页跳转")
    @GetMapping("/view")
    @RequiresPermissions("gen:sysStreet:view")
    public String view(ModelMap model) {
        String str = "街道设置";
        setTitle(model, new TitleVo("列表", str + "管理", true, "欢迎进入" + str + "页面", true, false));
        return prefix + "/list";
    }

    /**
     * list查询
     *
     * @param tablepar
     * @param searchText
     * @return
     */
    //@Log(title = "街道设置集合查询", action = "111")
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @PostMapping("/list")
    @RequiresPermissions("gen:sysStreet:list")
    @ResponseBody
    public Object list(Tablepar tablepar, String searchText) {
        PageInfo<SysStreetModel> page = sysStreetService.listTablepar(tablepar, "street_name", searchText);
        TableSplitResult<SysStreetModel> result = new TableSplitResult<SysStreetModel>(page.getPageNum(), page.getTotal(), page.getList());
        return result;
    }

    /**
     * 新增跳转
     *
     * @param modelMap
     * @return
     */
    @ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap) {
        return prefix + "/add";
    }

    /**
     * 新增保存
     *
     * @param sysStreet
     * @return
     * @author fuce
     * @Date 2019年11月11日 下午4:13:37
     */
    //@Log(title = "街道设置新增", action = "111")
    @ApiOperation(value = "新增", notes = "新增")
    @PostMapping("/add")
    @RequiresPermissions("gen:sysStreet:add")
    @ResponseBody
    public AjaxResult add(SysStreetModel sysStreet) {
        return toAjax(sysStreetService.save(sysStreet));
    }

    /**
     * 删除
     *
     * @param ids
     * @return
     */
    //@Log(title = "街道设置删除", action = "111")
    @ApiOperation(value = "删除", notes = "删除")
    @PostMapping("/remove")
    @RequiresPermissions("gen:sysStreet:remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(sysStreetService.removeByIds(StringUtils.splitList(ids, StringPool.COMMA)));
    }

    /**
     * 检查
     *
     * @param sysStreet
     * @return
     */
    @ApiOperation(value = "检查Name唯一", notes = "检查Name唯一")
    @PostMapping("/checkNameUnique")
    @ResponseBody
    public int checkNameUnique(SysStreetModel sysStreet) {
        int b = sysStreetService.checkNameUnique("street_name", sysStreet.getStreetName());
        return b > 0 ? 1 : 0;
    }


    /**
     * 修改跳转
     *
     * @param id
     * @param mmap
     * @return
     */
    @ApiOperation(value = "修改跳转", notes = "修改跳转")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap) {
        mmap.put("SysStreet", sysStreetService.getById(id));

        return prefix + "/edit";
    }

    /**
     * 修改保存
     *
     * @param record
     * @return
     */
    //@Log(title = "街道设置修改", action = "111")
    @ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("gen:sysStreet:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(SysStreetModel record) {
        return toAjax(sysStreetService.updateById(record));
    }


}
