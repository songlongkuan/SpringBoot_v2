package com.fc.test.controller.admin;

import com.fc.test.common.base.BaseController;
import com.fc.test.dao.model.SysAreaModel;
import com.fc.test.dao.model.SysCityModel;
import com.fc.test.dao.model.SysStreetModel;
import com.fc.test.dao.service.SysAreaService;
import com.fc.test.dao.service.SysCityService;
import com.fc.test.dao.service.SysProvinceService;
import com.fc.test.dao.service.SysStreetService;
import com.fc.test.model.custom.TitleVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 省份联动controller
 *
 * @author fuce
 * @ClassName: ProvinceLinkageController
 * @date 2019-10-05 11:19
 */
@Api(value = "省份联动controller")
@Controller
@RequestMapping("/ProvinceLinkageController")
public class ProvinceLinkageController extends BaseController {
    @Autowired
    private SysProvinceService sysProvinceService;
    @Autowired
    private SysCityService sysCityService;
    @Autowired
    private SysAreaService sysAreaService;
    @Autowired
    private SysStreetService sysStreetService;

    private String prefix = "admin/province";

    /**
     * 分页list页面
     *
     * @param model
     * @return
     */
    @ApiOperation(value = "分页跳转", notes = "分页跳转")
    @GetMapping("/view")
    public String view(ModelMap model) {
        String str = "省份联动";
        setTitle(model, new TitleVo("列表", str + "管理", true, "欢迎进入" + str + "页面", true, false));
        model.addAttribute("provinceList", sysProvinceService.list());
        return prefix + "/list";
    }

    /**
     * 市查询
     *
     * @param pid
     * @return
     */
    @ApiOperation(value = "市查询", notes = "市查询")
    @GetMapping("/getCity")
    @ResponseBody
    public List<SysCityModel> getCity(String pid) {
        return sysCityService.lambdaQuery().eq(SysCityModel::getProvinceCode, pid).list();
    }

    /**
     * 区查询
     * TODO(请说明这个方法的作用).
     *
     * @param pid
     * @return
     */
    @ApiOperation(value = "区查询", notes = "区查询")
    @GetMapping("/getArea")
    @ResponseBody
    public List<SysAreaModel> getArea(String pid) {
        return sysAreaService.getByCityCode(pid);

    }

    /**
     * 街道查询
     *
     * @param pid
     * @return
     */
    @ApiOperation(value = "街道查询", notes = "街道查询")
    @GetMapping("/getStreet")
    @ResponseBody
    public List<SysStreetModel> getStreet(String pid) {
        return sysStreetService.lambdaQuery().eq(SysStreetModel::getAreaCode,pid).list();
    }


}
