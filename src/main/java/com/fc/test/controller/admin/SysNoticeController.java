package com.fc.test.controller.admin;

import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.fc.test.common.base.BaseController;
import com.fc.test.common.base.PageInfo;
import com.fc.test.common.domain.AjaxResult;
import com.fc.test.dao.model.SysNoticeModel;
import com.fc.test.dao.service.SysNoticeService;
import com.fc.test.model.custom.TableSplitResult;
import com.fc.test.model.custom.Tablepar;
import com.fc.test.model.custom.TitleVo;
import com.fc.test.shiro.util.ShiroUtils;
import com.fc.test.util.StringUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**
 * 公告Controller
 *
 * @author fuce
 * @ClassName: SysNoticeController
 * @date 2019-11-20 22:31
 */
@Api(value = "公告")
@Controller
@RequestMapping("/SysNoticeController")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SysNoticeController extends BaseController {
    private final SysNoticeService sysNoticeService;

    private String prefix = "admin/sysNotice";


    /**
     * 展示页面跳转
     *
     * @param model
     * @return
     * @author fuce
     * @Date 2019年11月11日 下午4:09:24
     */
    @ApiOperation(value = "分页跳转", notes = "分页跳转")
    @GetMapping("/view")
    @RequiresPermissions("gen:sysNotice:view")
    public String view(ModelMap model) {
        String str = "公告";
        setTitle(model, new TitleVo("列表", str + "管理", true, "欢迎进入" + str + "页面", true, false));
        return prefix + "/list";
    }

    /**
     * list页面
     *
     * @param tablepar
     * @param searchText
     * @return
     * @author fuce
     * @Date 2019年11月11日 下午4:09:35
     */
    //@Log(title = "公告集合查询", action = "111")
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @PostMapping("/list")
    @RequiresPermissions("gen:sysNotice:list")
    @ResponseBody
    public Object list(Tablepar tablepar, String searchText) {
        PageInfo<SysNoticeModel> page = sysNoticeService.listTablepar(tablepar, searchText);
        return new TableSplitResult<SysNoticeModel>(page.getPageNum(), page.getTotal(), page.getList());
    }


    /**
     * 对应的用户的展示页面
     *
     * @param model
     * @return
     * @author fuce
     * @Date 2019年11月11日 下午4:09:42
     */
    @ApiOperation(value = "对应的用户的展示页面", notes = "对应的用户的展示页面")
    @GetMapping("/viewUser")
    public String viewUser(ModelMap model) {
        String str = "公告";
        setTitle(model, new TitleVo("列表", str + "管理", true, "欢迎进入" + str + "页面", true, false));
        return prefix + "/list_view";
    }

    /**
     * 根据公告id查询跳转到公告详情页面
     *
     * @return
     */
    @ApiOperation(value = "table根据公告id查询跳转到公告详情页面", notes = "table根据公告id查询跳转到公告详情页面")
    @PostMapping("/viewUserlist")
    @ResponseBody
    public Object viewUserlist(Tablepar tablepar, String searchText) {
        PageInfo<SysNoticeModel> sysNoticeModelPageInfo = sysNoticeService.listTablepar(ShiroUtils.getUser(), tablepar, searchText);
        TableSplitResult<SysNoticeModel> result = new TableSplitResult<SysNoticeModel>(sysNoticeModelPageInfo.getPageNum(), sysNoticeModelPageInfo.getTotal(), sysNoticeModelPageInfo.getList());
        return result;
    }

    /**
     * 新增跳转
     *
     * @param modelMap
     * @return
     */
    @ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap) {
        return prefix + "/add";
    }

    /**
     * 新增保存
     *
     * @param sysNotice
     * @return
     * @author fuce
     * @Date 2019年11月11日 下午4:07:09
     */
    //@Log(title = "公告新增", action = "111")
    @ApiOperation(value = "新增", notes = "新增")
    @PostMapping("/add")
    @RequiresPermissions("gen:sysNotice:add")
    @ResponseBody
    public AjaxResult add(SysNoticeModel sysNotice) {
        return sysNoticeService.save(sysNotice) ? success() : error();
    }

    /**
     * 删除
     *
     * @param ids
     * @return
     **/
    //@Log(title = "公告删除", action = "111")
    @ApiOperation(value = "删除", notes = "删除")
    @PostMapping("/remove")
    @RequiresPermissions("gen:sysNotice:remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        return sysNoticeService.removeByIds(StringUtils.splitList(ids, StringPool.COMMA)) ? success() : error();
    }

    /**
     * 检查
     *
     * @param sysNotice
     * @return
     */
    @ApiOperation(value = "检查Name唯一", notes = "检查Name唯一")
    @PostMapping("/checkNameUnique")
    @ResponseBody
    public int checkNameUnique(SysNoticeModel sysNotice) {
        return sysNoticeService.checkNameUnique(sysNotice) > 0 ? 1 : 0;
    }

    /**
     * 根据公告id查询跳转到公告详情页面
     *
     * @param id
     * @param mmap
     * @return
     */
    //@Log(title = "字典数据表删除", action = "1")
    @ApiOperation(value = "根据公告id查询跳转到公告详情页面", notes = " 根据公告id查询跳转到公告详情页面")
    @GetMapping("/viewinfo/{id}")
    public String viewinfo(@PathVariable("id") String id, ModelMap mmap) {

        String str = "公告";
        setTitle(mmap, new TitleVo("详情", str + "列表", true, "欢迎进入" + str + "详情页面", true, false));
        SysNoticeModel notice = sysNoticeService.getById(id);
        mmap.addAttribute("notice", notice);
        //把推送给该用户的公告设置为已读
        sysNoticeService.editUserState(id);
        return prefix + "/view";
    }


    /**
     * 修改跳转
     *
     * @param id
     * @param mmap
     * @return
     */
    @ApiOperation(value = "修改跳转", notes = "修改跳转")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap) {
        mmap.put("SysNotice", sysNoticeService.getById(id));

        return prefix + "/edit";
    }

    /**
     * 修改保存
     */
    //@Log(title = "公告修改", action = "111")
    @ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("gen:sysNotice:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(SysNoticeModel record) {
        return toAjax(sysNoticeService.updateById(record));
    }
}
