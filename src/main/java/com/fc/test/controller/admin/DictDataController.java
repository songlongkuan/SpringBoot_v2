package com.fc.test.controller.admin;


import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.fc.test.common.base.PageInfo;
import com.fc.test.dao.model.SysDictDataModel;
import com.fc.test.dao.service.SysDictDataService;
import com.fc.test.dao.service.SysDictTypeService;
import com.fc.test.util.StringUtils;
import lombok.RequiredArgsConstructor;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fc.test.common.base.BaseController;
import com.fc.test.common.domain.AjaxResult;
import com.fc.test.model.custom.TableSplitResult;
import com.fc.test.model.custom.Tablepar;
import com.fc.test.model.custom.TitleVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 字典表Controller
 *
 * @author fuce
 * @ClassName: DictDataController
 * @date 2019-11-20 22:46
 */
@Api(value = "字典数据表")
@Controller
@RequestMapping("/DictDataController")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class DictDataController extends BaseController {

    private String prefix = "admin/dict_data";
    private final SysDictDataService tSysDictDataService;
    private final SysDictTypeService sysDictTypeService;

    /**
     * 分页list页面
     *
     * @param model
     * @param dictId
     * @return
     */
    @ApiOperation(value = "分页跳转", notes = "分页跳转")
    @GetMapping("/view")
    @RequiresPermissions("system:dictData:view")
    public String view(ModelMap model, String dictId) {
        model.addAttribute("dictId", dictId);
        String str = "字典数据表";
        setTitle(model, new TitleVo("列表", str + "管理", true, "欢迎进入" + str + "页面", true, false));
        return prefix + "/list";
    }

    /**
     * 字典数据表集合查询
     *
     * @param tablepar
     * @param searchText
     * @param dictId
     * @return
     */
    //@Log(title = "字典数据表集合查询", action = "1")
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @PostMapping("/list")
    @RequiresPermissions("system:dictData:list")
    @ResponseBody
    public Object list(Tablepar tablepar, String searchText, String dictId) {
        final String dictType = sysDictTypeService.getById(dictId).getDictType();
        PageInfo<SysDictDataModel> page = tSysDictDataService.listTablepar(tablepar, "dict_value", searchText, queryWrapper -> {
            queryWrapper.eq("dict_type", dictType);
        });
        TableSplitResult<SysDictDataModel> result = new TableSplitResult<SysDictDataModel>(page.getPageNum(), page.getTotal(), page.getList());
        return result;
    }

    /**
     * 新增跳转
     *
     * @param modelMap
     * @param dictId
     * @return
     */
    @ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap, String dictId) {
        modelMap.addAttribute("dictType", sysDictTypeService.getById(dictId).getDictType());
        return prefix + "/add";
    }

    /**
     * 新增保存
     *
     * @param tSysDictData
     * @param model
     * @return
     */
    //@Log(title = "字典数据表新增", action = "1")
    @ApiOperation(value = "新增", notes = "新增")
    @PostMapping("/add")
    @RequiresPermissions("system:dictData:add")
    @ResponseBody
    public AjaxResult add(SysDictDataModel tSysDictData, Model model) {
        return toAjax(tSysDictDataService.save(tSysDictData));
    }

    /**
     * 删除
     *
     * @param ids
     * @return
     */
    //@Log(title = "字典数据表删除", action = "1")
    @ApiOperation(value = "删除", notes = "删除")
    @PostMapping("/remove")
    @RequiresPermissions("system:dictData:remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(tSysDictDataService.removeByIds(StringUtils.splitList(ids, StringPool.COMMA)));
    }

    /**
     * 检查
     *
     * @return
     */
    @ApiOperation(value = "检查Name唯一", notes = "检查Name唯一")
    @PostMapping("/checkNameUnique")
    @ResponseBody
    public int checkNameUnique(SysDictDataModel tSysDictData) {
        int b = tSysDictDataService.checkNameUnique("dict_value", tSysDictData.getDictValue());
        return b > 0 ? 1 : 0;
    }


    /**
     * 修改跳转
     *
     * @param id
     * @param mmap
     * @return
     */
    @ApiOperation(value = "修改跳转", notes = "修改跳转")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap) {
        SysDictDataModel sysDictData = tSysDictDataService.getById(id);
        mmap.put("TSysDictData", sysDictData);
        return prefix + "/edit";
    }

    /**
     * 修改保存
     */
    //@Log(title = "字典数据表修改", action = "1")
    @ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("system:dictData:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(SysDictDataModel record) {
        return toAjax(tSysDictDataService.updateById(record));
    }


}
