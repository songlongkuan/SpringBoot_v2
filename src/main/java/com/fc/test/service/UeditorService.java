package com.fc.test.service;

import com.fc.test.common.conf.V2Config;
import com.fc.test.common.file.FileUploadUtils;
import com.fc.test.dao.model.SysDatasModel;
import com.fc.test.dao.model.SysFileDataModel;
import com.fc.test.dao.model.SysFileModel;
import com.fc.test.dao.service.SysDatasService;
import com.fc.test.dao.service.SysFileDataService;
import com.fc.test.dao.service.SysFileService;
import com.fc.test.shiro.util.ShiroUtils;
import com.fc.test.util.SnowflakeIdWorker;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Date;

/**
 * 百度文件上传service
 *
 * @ClassName: UeditorService
 * @author: fc
 * @date: 2019年6月30日 下午5:51:43
 */
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Service
public class UeditorService {
    /**
     * 文件存储
     */
    private final SysFileDataService sysFileDataService;
    /**
     * 文件存储服务
     */
    private final SysDatasService sysDatasService;
    /**
     * 文件信息
     */
    private final SysFileService sysFileService;


    /**
     * 文件上传文件存储到文件表
     *
     * @return TsysDatas
     * @throws IOException
     */
    public SysDatasModel fileDataByinsert(MultipartFile file) throws IOException {
        //文件上传获取文件名字
        String files = FileUploadUtils.upload(file);
        //补充完整url地址 
        String filesURL = "";
        if ("Y".equals(V2Config.getIsstatic())) {
            filesURL = V2Config.getIsroot_dir() + files;
        } else {
            filesURL = V2Config.getProfile() + files;
        }


        SysDatasModel record = new SysDatasModel();
        //添加雪花主键id
        record.setId(SnowflakeIdWorker.getUUID());
        record.setFilePath(filesURL);
        if (sysDatasService.save(record)) {
            return record;
        }
        return null;
    }


    /**
     * 添加文件信息
     */
    @Transactional
    public SysFileDataModel fileInfoByininsert(String dataId) {
        SysFileModel record = new SysFileModel();
        record.setFileName("百度富文本上传");
        //插入创建人id
        record.setCreateUserId(ShiroUtils.getUserId());
        //插入创建人name
        record.setCreateUserName(ShiroUtils.getLoginName());
        //插入创建时间
        record.setCreateTime(new Date());
        //添加雪花主键id
        record.setId(SnowflakeIdWorker.getUUID());
        //插入关联表
        SysFileDataModel tsysFileData = new SysFileDataModel();
        tsysFileData.setId(SnowflakeIdWorker.getUUID());
        tsysFileData.setFileId(record.getId());
        tsysFileData.setDataId(dataId);
        sysFileDataService.save(tsysFileData);
        if (sysFileService.save(record)) {
            return tsysFileData;
        } else {
            return null;
        }
    }
}
