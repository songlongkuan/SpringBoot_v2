package com.fc.test.dao.model;

import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.apache.ibatis.annotations.Arg;

/**
 * <p>
 * 用户角色中间表
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_sys_role_user")
@AllArgsConstructor
@NoArgsConstructor
public class SysRoleUserModel implements Serializable {

    private static final long serialVersionUID = 1L;
    private String id;
    /**
     * 用户id
     */
    private String sysUserId;

    /**
     * 角色id
     */
    private String sysRoleId;


}
