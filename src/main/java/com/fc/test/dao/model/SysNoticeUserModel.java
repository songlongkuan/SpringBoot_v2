package com.fc.test.dao.model;

import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 公告_用户外键
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_sys_notice_user")
public class SysNoticeUserModel implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 公告id
     */
    private String noticeId;

    /**
     * 用户id
     */
    private String userId;

    /**
     * 0未阅读 1 阅读
     */
    private Integer state;


}
