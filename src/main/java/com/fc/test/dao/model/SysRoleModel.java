package com.fc.test.dao.model;

import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * <p>
 * 角色表
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_sys_role")
@NoArgsConstructor
@AllArgsConstructor
public class SysRoleModel implements Serializable {

    private static final long serialVersionUID = 1L;
    private String id;
    /**
     * 角色名称
     */
    private String name;


}
