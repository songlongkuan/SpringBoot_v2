package com.fc.test.dao.service.impl;

import com.fc.test.dao.model.SysDictDataModel;
import com.fc.test.dao.mapper.SysDictDataMapper;
import com.fc.test.dao.service.SysDictDataService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 字典数据表 服务实现类
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
@Service
public class SysDictDataServiceImpl extends ServiceImpl<SysDictDataMapper, SysDictDataModel> implements SysDictDataService {

}
