package com.fc.test.dao.service;

import com.fc.test.common.base.BaseIService;
import com.fc.test.dao.model.SysInterUrlModel;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 拦截url表 服务类
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
public interface SysInterUrlService extends BaseIService<SysInterUrlModel> {

}
