package com.fc.test.dao.service.impl;

import com.fc.test.dao.model.SysDepartmentModel;
import com.fc.test.dao.mapper.SysDepartmentMapper;
import com.fc.test.dao.service.SysDepartmentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fc.test.model.custom.BootstrapTree;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 部门表 服务实现类
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
@Service
public class SysDepartmentServiceImpl extends ServiceImpl<SysDepartmentMapper, SysDepartmentModel> implements SysDepartmentService {

    @Override
    public BootstrapTree getbooBootstrapTreePerm() {
        List<BootstrapTree> treeList = new ArrayList<BootstrapTree>();
        List<SysDepartmentModel> menuList = getall();
        treeList = getbooBootstrapTreePerm(menuList, "0");
        if (treeList != null && treeList.size() == 1) {
            return treeList.get(0);
        }
        return new BootstrapTree("菜单", "fa fa-home", "", "-1", "###", 0, treeList, "", 0);
    }

    /**
     * 获取树
     * @param menuList
     * @param parentId
     * @return
     */
    private static List<BootstrapTree> getbooBootstrapTreePerm(List<SysDepartmentModel> menuList, String parentId){
        List<BootstrapTree> treeList = new ArrayList<>();
        List<BootstrapTree> childList = null;
        for(SysDepartmentModel p : menuList) {
            p.setParentId(p.getParentId()==null||p.getParentId().trim().equals("")? "0":p.getParentId());
            if(p.getParentId().trim().equals(parentId)) {
                if(p.getChildCount()!=null&&p.getChildCount()>0) {
                    childList = getbooBootstrapTreePerm(menuList, String.valueOf(p.getId()));
                }
                BootstrapTree bootstrapTree = new BootstrapTree(p.getDeptName(), "", "", String.valueOf(p.getId()), "",0,childList,p.getDeptName(),p.getStatus());
                treeList.add(bootstrapTree);
                childList = null;
            }
        }
        return treeList.size() >0 ? treeList : null;
    }

    /**
     * 根据用户id获取用户角色如果用户为null 获取所有权限
     *
     * @return
     */
    public List<SysDepartmentModel> getall() {
        return lambdaQuery().orderByAsc(SysDepartmentModel::getOrderNum).list();
    }
}
