package com.fc.test.dao.service;

import com.fc.test.common.base.BaseIService;
import com.fc.test.dao.model.SysRoleModel;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
public interface SysRoleService extends BaseIService<SysRoleModel> {
    /**
     * 添加角色绑定权限
     *
     * @param record 角色信息
     * @param prem   权限id集合
     * @return
     */
    boolean insertRoleAndPrem(SysRoleModel record, String prem);

    /**
     * 修改用户角色 以及下面绑定的权限
     *
     * @param record
     * @return
     */
    boolean updateRoleAndPrem(SysRoleModel record, String prem);

    /**
     * 根据用户id查询角色
     * @param uid
     * @return
     */
    List<SysRoleModel> queryUserRole(String uid);
}
