package com.fc.test.dao.service.impl;

import com.fc.test.dao.model.SysAreaModel;
import com.fc.test.dao.mapper.SysAreaMapper;
import com.fc.test.dao.service.SysAreaService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 地区设置 服务实现类
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
@Service
public class SysAreaServiceImpl extends ServiceImpl<SysAreaMapper, SysAreaModel> implements SysAreaService {

    @Override
    public List<SysAreaModel> getByCityCode(String cityCode) {
        return lambdaQuery().eq(SysAreaModel::getCityCode, cityCode).list();
    }
}
