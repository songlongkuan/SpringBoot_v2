package com.fc.test.dao.service.impl;

import com.fc.test.dao.model.SysPositionModel;
import com.fc.test.dao.mapper.SysPositionMapper;
import com.fc.test.dao.service.SysPositionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 岗位表 服务实现类
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
@Service
public class SysPositionServiceImpl extends ServiceImpl<SysPositionMapper, SysPositionModel> implements SysPositionService {

}
