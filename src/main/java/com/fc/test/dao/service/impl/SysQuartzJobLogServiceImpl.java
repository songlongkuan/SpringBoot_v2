package com.fc.test.dao.service.impl;

import com.fc.test.dao.model.SysQuartzJobLogModel;
import com.fc.test.dao.mapper.SysQuartzJobLogMapper;
import com.fc.test.dao.service.SysQuartzJobLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 定时任务调度日志表 服务实现类
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
@Service
public class SysQuartzJobLogServiceImpl extends ServiceImpl<SysQuartzJobLogMapper, SysQuartzJobLogModel> implements SysQuartzJobLogService {

}
