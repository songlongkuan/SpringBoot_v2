package com.fc.test.dao.service;

import com.fc.test.common.base.BaseIService;
import com.fc.test.dao.model.SysCityModel;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 城市设置 服务类
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
public interface SysCityService extends BaseIService<SysCityModel> {

}
