package com.fc.test.dao.service.impl;

import com.fc.test.dao.model.SysOperLogModel;
import com.fc.test.dao.mapper.SysOperLogMapper;
import com.fc.test.dao.service.SysOperLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 日志记录表 服务实现类
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
@Service
public class SysOperLogServiceImpl extends ServiceImpl<SysOperLogMapper, SysOperLogModel> implements SysOperLogService {

}
