package com.fc.test.dao.service.impl;

import com.fc.test.dao.model.SysDictTypeModel;
import com.fc.test.dao.mapper.SysDictTypeMapper;
import com.fc.test.dao.service.SysDictTypeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 字典类型表 服务实现类
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
@Service
public class SysDictTypeServiceImpl extends ServiceImpl<SysDictTypeMapper, SysDictTypeModel> implements SysDictTypeService {

}
