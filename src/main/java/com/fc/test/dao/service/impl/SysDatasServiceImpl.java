package com.fc.test.dao.service.impl;

import com.fc.test.common.conf.V2Config;
import com.fc.test.common.file.FileUploadUtils;
import com.fc.test.dao.model.SysDatasModel;
import com.fc.test.dao.mapper.SysDatasMapper;
import com.fc.test.dao.service.SysDatasService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fc.test.util.SnowflakeIdWorker;
import com.fc.test.util.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * <p>
 * 文件表存储表 服务实现类
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
@Service
public class SysDatasServiceImpl extends ServiceImpl<SysDatasMapper, SysDatasModel> implements SysDatasService {

    @Override
    public String insertSelective(MultipartFile file) throws IOException {
        //文件上传获取文件名字
        String files = FileUploadUtils.upload(file);
        //补充完整url地址
        String filesURL = "";
        if ("Y".equals(V2Config.getIsstatic())) {
            filesURL = V2Config.getIsroot_dir() + files;
        } else {
            filesURL = V2Config.getProfile() + files;
        }
        String fileName = file.getOriginalFilename();
        // 获得文件后缀名称
        String suffixName = fileName.substring(fileName.lastIndexOf(".")).toLowerCase();
        if (StringUtils.isEmpty(suffixName)) {
            //如果没有后缀默认后缀
            suffixName = FileUploadUtils.IMAGE_JPG_EXTENSION;
        }

        SysDatasModel record = new SysDatasModel();
        //添加雪花主键id
        record.setId(SnowflakeIdWorker.getUUID());
        record.setFilePath(filesURL);
        record.setFileSuffix(suffixName);
        if (save(record)) {
            return record.getId();
        } else {
            return null;
        }
    }
}
