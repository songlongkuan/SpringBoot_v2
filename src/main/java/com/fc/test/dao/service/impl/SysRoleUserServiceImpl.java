package com.fc.test.dao.service.impl;

import com.fc.test.dao.model.SysRoleUserModel;
import com.fc.test.dao.mapper.SysRoleUserMapper;
import com.fc.test.dao.service.SysRoleUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户角色中间表 服务实现类
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
@Service
public class SysRoleUserServiceImpl extends ServiceImpl<SysRoleUserMapper, SysRoleUserModel> implements SysRoleUserService {

}
