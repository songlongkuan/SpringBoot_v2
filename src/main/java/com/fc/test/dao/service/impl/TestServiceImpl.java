package com.fc.test.dao.service.impl;

import com.fc.test.dao.model.TestModel;
import com.fc.test.dao.mapper.TestMapper;
import com.fc.test.dao.service.TestService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 测试表 服务实现类
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
@Service
public class TestServiceImpl extends ServiceImpl<TestMapper, TestModel> implements TestService {

}
