package com.fc.test.dao.service.impl;

import cn.hutool.core.util.RandomUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fc.test.common.support.Convert;
import com.fc.test.dao.mapper.SysRoleMapper;
import com.fc.test.dao.model.SysPermissionRoleModel;
import com.fc.test.dao.model.SysRoleModel;
import com.fc.test.dao.service.SysPermissionRoleService;
import com.fc.test.dao.service.SysRoleService;
import com.fc.test.util.SnowflakeIdWorker;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;

/**
 * <p>
 * 角色表 服务实现类
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRoleModel> implements SysRoleService {
    private final SysPermissionRoleService sysPermissionRoleService;

    @Transactional
    @Override
    public boolean insertRoleAndPrem(SysRoleModel record, String prem) {
        //添加雪花主键id
        String roleid = SnowflakeIdWorker.getUUID();
        record.setId(roleid);
        //添加权限
        List<String> prems = Convert.toListStrArray(prem);
        for (String premid : prems) {
            SysPermissionRoleModel tsysPermissionRole = new SysPermissionRoleModel(RandomUtil.randomUUID(), roleid, premid);
            sysPermissionRoleService.save(tsysPermissionRole);
        }
        return save(record);
    }

    @Transactional
    @Override
    public boolean updateRoleAndPrem(SysRoleModel record, String prem) {
        boolean remove = sysPermissionRoleService.lambdaUpdate()
                .eq(SysPermissionRoleModel::getRoleId, record.getId())
                .remove();
        List<String> prems = Convert.toListStrArray(prem);
        if (prems.isEmpty()) return true;
        LinkedList<SysPermissionRoleModel> sysPermissionRoleModels = new LinkedList<SysPermissionRoleModel>();
        prems.forEach(it -> {
            sysPermissionRoleModels.add(new SysPermissionRoleModel(RandomUtil.randomUUID(), record.getId(), it));
        });
       return sysPermissionRoleService.saveBatch(sysPermissionRoleModels);
    }

    @Override
    public List<SysRoleModel> queryUserRole(String uid) {
        return baseMapper.queryUserRole(uid);
    }
}
