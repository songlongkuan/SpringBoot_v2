package com.fc.test.dao.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fc.test.common.base.PageInfo;
import com.fc.test.common.interceptor.ListTableparInerceptor;
import com.fc.test.dao.model.SysEmailModel;
import com.fc.test.dao.mapper.SysEmailMapper;
import com.fc.test.dao.service.SysEmailService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fc.test.model.custom.Tablepar;
import com.fc.test.util.StringUtils;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 电子邮件 服务实现类
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
@Service
public class SysEmailServiceImpl extends ServiceImpl<SysEmailMapper, SysEmailModel> implements SysEmailService {

//    @Override
//    public <T> PageInfo<SysEmailModel> listTablepar(Tablepar tablepar, String column, String searchText, ListTableparInerceptor<SysEmailModel> listTableparInerceptor) {
//        System.out.println("SysEmailServiceImpl........");
//        QueryWrapper<SysEmailModel> queryWrapper = new QueryWrapper<>();
//        queryWrapper.orderByAsc("id");
//        if (StringUtils.isNotBlank(searchText)) {
//            queryWrapper.like(column, searchText);
//        }
//
//        if (listTableparInerceptor != null) listTableparInerceptor.call(queryWrapper);
//
//        if (StringUtils.isNotBlank(tablepar.getOrderByColumn())) {
//            String underColumn = StringUtils.toUnderScoreCase(tablepar.getOrderByColumn());
//            queryWrapper.orderBy(true, "asc".equals(tablepar.getIsAsc()), underColumn);
//        }
//        Page<SysEmailModel> page = page(new Page<>(tablepar.getPageNum(), tablepar.getPageSize()), queryWrapper);
//        return iPageToPageInfo(page);
//    }
}
