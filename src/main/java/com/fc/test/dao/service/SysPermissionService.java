package com.fc.test.dao.service;

import com.fc.test.common.base.BaseIService;
import com.fc.test.dao.model.SysPermissionModel;
import com.baomidou.mybatisplus.extension.service.IService;
import com.fc.test.model.custom.BootstrapTree;

import java.util.List;

/**
 * <p>
 * 权限表 服务类
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
public interface SysPermissionService extends BaseIService<SysPermissionModel> {
    /**
     * 获取转换成bootstarp的权限数据
     *
     * @return
     */
    BootstrapTree getbooBootstrapTreePerm(String userId);

    /**
     * 根据用户id获取用户角色如果用户为null 获取所有权限
     *
     * @return
     */
    List<SysPermissionModel> getall(String userid);

    /**
     * 根据用户id查询出用户的所有权限
     *
     * @param userid
     * @return
     */
    List<SysPermissionModel> findByAdminUserId(String userid);

    List<SysPermissionModel> list2(String searchText);

    /**
     * 根据权限字段查询是否存在
     * @param perms
     * @return
     */
    boolean queryLikePerms(String perms);

    /**
     * 根据角色id查询权限
     * @param rolid
     * @return
     */
    List<SysPermissionModel> queryRoleId(String rolid);

    int deleteByPrimaryKey(String ids);

    /**
     * 获取角色已有的Bootstarp权限
     * @param roleId
     * @return
     */
    BootstrapTree getCheckPrem(String roleId);
}
