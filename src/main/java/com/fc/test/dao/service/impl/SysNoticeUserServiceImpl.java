package com.fc.test.dao.service.impl;

import com.fc.test.dao.model.SysNoticeUserModel;
import com.fc.test.dao.mapper.SysNoticeUserMapper;
import com.fc.test.dao.service.SysNoticeUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 公告_用户外键 服务实现类
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
@Service
public class SysNoticeUserServiceImpl extends ServiceImpl<SysNoticeUserMapper, SysNoticeUserModel> implements SysNoticeUserService {

}
