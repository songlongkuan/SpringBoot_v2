package com.fc.test.dao.service;

import com.fc.test.dao.model.SysRoleUserModel;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户角色中间表 服务类
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
public interface SysRoleUserService extends IService<SysRoleUserModel> {

}
