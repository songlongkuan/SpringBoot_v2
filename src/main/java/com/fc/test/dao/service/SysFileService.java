package com.fc.test.dao.service;

import com.fc.test.common.base.BaseIService;
import com.fc.test.dao.model.SysFileModel;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 文件信息表 服务类
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
public interface SysFileService extends BaseIService<SysFileModel> {

    boolean insertSelective(SysFileModel file, String dataId);

    /**
     * 删除文件存储表以及数据库
     * @param ids   文件集合 1,2,3
     * @return
     */
    boolean deleteBydataFile(String ids);

    /**
     * 修改信息
     * @param tsysFile
     * @param dataId
     * @return
     */
    boolean updateByPrimaryKey(SysFileModel tsysFile, String dataId);
}
