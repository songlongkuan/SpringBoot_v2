package com.fc.test.dao.service.impl;

import com.fc.test.dao.model.SysCityModel;
import com.fc.test.dao.mapper.SysCityMapper;
import com.fc.test.dao.service.SysCityService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 城市设置 服务实现类
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
@Service
public class SysCityServiceImpl extends ServiceImpl<SysCityMapper, SysCityModel> implements SysCityService {

}
