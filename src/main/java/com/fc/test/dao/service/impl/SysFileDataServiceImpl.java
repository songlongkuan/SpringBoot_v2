package com.fc.test.dao.service.impl;

import com.fc.test.dao.model.SysDatasModel;
import com.fc.test.dao.model.SysFileDataModel;
import com.fc.test.dao.mapper.SysFileDataMapper;
import com.fc.test.dao.service.SysDatasService;
import com.fc.test.dao.service.SysFileDataService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 文件数据外键绑定表 服务实现类
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SysFileDataServiceImpl extends ServiceImpl<SysFileDataMapper, SysFileDataModel> implements SysFileDataService {
    private final SysDatasService sysDatasService;

    @Override
    public List<SysDatasModel> queryfileID(String fileid) {
        List<SysFileDataModel> sysFileDataModels = lambdaQuery().eq(SysFileDataModel::getFileId, fileid).list();
        if (sysFileDataModels.isEmpty()) return Collections.emptyList();
        Set<String> dataIds = sysFileDataModels.stream().map(SysFileDataModel::getDataId).collect(Collectors.toSet());
        return sysDatasService.lambdaQuery().in(SysDatasModel::getId, dataIds).list();

    }
}
