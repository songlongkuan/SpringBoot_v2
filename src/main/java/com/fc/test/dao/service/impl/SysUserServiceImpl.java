package com.fc.test.dao.service.impl;

import com.fc.test.dao.model.SysRoleModel;
import com.fc.test.dao.model.SysRoleUserModel;
import com.fc.test.dao.model.SysUserModel;
import com.fc.test.dao.mapper.SysUserMapper;
import com.fc.test.dao.service.SysRoleService;
import com.fc.test.dao.service.SysRoleUserService;
import com.fc.test.dao.service.SysUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fc.test.model.custom.RoleVo;
import com.fc.test.util.MD5Util;
import com.fc.test.util.SnowflakeIdWorker;
import com.fc.test.util.StringUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUserModel> implements SysUserService {
    private final SysRoleUserService sysRoleUserService;
    private final SysRoleService sysRoleService;

    @Override
    public SysUserModel queryUserName(String username) {
        return lambdaQuery().eq(SysUserModel::getUsername, username).one();
    }

    @Override
    public boolean insertUserRoles(SysUserModel record, List<String> roles) {
        String userid = SnowflakeIdWorker.getUUID();
        record.setId(userid);
        if (StringUtils.isNotEmpty(roles)) {
            for (String rolesid : roles) {
                SysRoleUserModel roleUser = new SysRoleUserModel(SnowflakeIdWorker.getUUID(), userid, rolesid);
                sysRoleUserService.save(roleUser);
            }
        }

        //密码加密
        record.setPassword(MD5Util.encode(record.getPassword()));
        return save(record);
    }

    @Override
    public List<RoleVo> getUserIsRole(String userid) {
        List<RoleVo> list = new ArrayList<RoleVo>();
        //查询出我的权限
        List<SysRoleModel> myRoles = sysRoleService.queryUserRole(userid);
        //查询系统所有的角色
        List<SysRoleModel> sysRoleModels = sysRoleService.list();
        if (sysRoleModels.isEmpty()) return Collections.emptyList();
        sysRoleModels.forEach(it -> {
            boolean isflag = false;
            RoleVo roleVo = new RoleVo(it.getId(), it.getName(), isflag);
            for (SysRoleModel myRole : myRoles) {
                if (it.getId().equals(myRole.getId())) {
                    isflag = true;
                    break;
                }
            }
            if (isflag) {
                roleVo.setIscheck(true);
                list.add(roleVo);
            } else {
                list.add(roleVo);
            }
        });
        return list;
    }

    @Override
    public boolean updateUserRoles(SysUserModel record, List<String> roles) {
        //先删除这个用户的所有角色
        sysRoleUserService.lambdaUpdate().eq(SysRoleUserModel::getSysUserId, record.getId()).remove();
        if (roles == null || roles.isEmpty()) return true;

        List<SysRoleUserModel> newRoles = new ArrayList<>(roles.size());
        roles.forEach(it -> {
            SysRoleUserModel sysRoleUserModel = new SysRoleUserModel(SnowflakeIdWorker.getUUID(), record.getId(), it);
            newRoles.add(sysRoleUserModel);
        });
        //添加新的角色信息
        return sysRoleUserService.saveBatch(newRoles);
    }
}
