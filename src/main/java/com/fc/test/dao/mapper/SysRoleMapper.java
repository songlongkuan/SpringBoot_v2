package com.fc.test.dao.mapper;

import com.fc.test.dao.model.SysRoleModel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 角色表 Mapper 接口
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
public interface SysRoleMapper extends BaseMapper<SysRoleModel> {

    List<SysRoleModel> queryUserRole(@Param("userid") String uid);
}
