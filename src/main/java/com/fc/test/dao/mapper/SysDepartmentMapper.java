package com.fc.test.dao.mapper;

import com.fc.test.dao.model.SysDepartmentModel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 部门表 Mapper 接口
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
public interface SysDepartmentMapper extends BaseMapper<SysDepartmentModel> {

}
