package com.fc.test.dao.mapper;

import com.fc.test.dao.model.TestModel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 测试表 Mapper 接口
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
public interface TestMapper extends BaseMapper<TestModel> {

}
