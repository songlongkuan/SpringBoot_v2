package com.fc.test.dao.mapper;

import com.fc.test.dao.model.SysDictDataModel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 字典数据表 Mapper 接口
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
public interface SysDictDataMapper extends BaseMapper<SysDictDataModel> {

}
