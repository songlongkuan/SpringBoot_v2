package com.fc.test.dao.mapper;

import com.fc.test.dao.model.SysOperLogModel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 日志记录表 Mapper 接口
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
public interface SysOperLogMapper extends BaseMapper<SysOperLogModel> {

}
