package com.fc.test.dao.mapper;

import com.fc.test.dao.model.SysDatasModel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 文件表存储表 Mapper 接口
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
public interface SysDatasMapper extends BaseMapper<SysDatasModel> {

}
