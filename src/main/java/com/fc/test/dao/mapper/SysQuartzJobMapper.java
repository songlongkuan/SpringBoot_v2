package com.fc.test.dao.mapper;

import com.fc.test.dao.model.SysQuartzJobModel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 定时任务调度表 Mapper 接口
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
public interface SysQuartzJobMapper extends BaseMapper<SysQuartzJobModel> {

}
