package com.fc.test.dao.mapper;

import com.fc.test.dao.model.SysRoleUserModel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户角色中间表 Mapper 接口
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
public interface SysRoleUserMapper extends BaseMapper<SysRoleUserModel> {

}
