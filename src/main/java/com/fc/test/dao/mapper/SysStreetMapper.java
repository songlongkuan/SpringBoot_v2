package com.fc.test.dao.mapper;

import com.fc.test.dao.model.SysStreetModel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 街道设置 Mapper 接口
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
public interface SysStreetMapper extends BaseMapper<SysStreetModel> {

}
