package com.fc.test.dao.mapper;

import com.fc.test.dao.model.SysPermissionRoleModel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色权限中间表 Mapper 接口
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
public interface SysPermissionRoleMapper extends BaseMapper<SysPermissionRoleModel> {

}
